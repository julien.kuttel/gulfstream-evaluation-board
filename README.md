# GULFstream Evaluation Board


## GULFstream evaluation board, Firmware, Software and Jupyter Notebook
This GitLab repository includes different projects in the master branch.

FW_SW_GULF: Firmware and Software for testing the GULFstream ASIC, (Vivado)

FW_SW_Test: Firmware and Software for testing the setup with two Pynq (Simulate GULF), (Vivado)

Jupyter_Eval_Board: Jupyter Notebook files to communicate with the evaluation board and show the incoming data

Tutorial_PL_PS: A tutorial to set up a data transfer from the PL to the PS, (Vivado)

Jupyter_Tutorial: A tutorial to set up a data transfer from the PL to the PS, (Jupyter Notebook)

ip_repo: All the Custom-made IP-Block for data transfer (Used in all Vivado Projects)

## Description
The projects, FW_SW_GULF and Jupyter_Eval_Board, are ready to test the GULFstream Chip on the evaluation board.

The project FW_SW_Test is to verify the setup with two Pynq, where one simulates the GULFstream. The Jupyter_Eval_Board project is also used in this case.

The projects Tutorial_PL_PS and Jupyter_Tutorial are ready to set up a data transfer from the PL to the PS and show the incoming data in Jupyter Notebook.

## Installation
When using the Pynq board for the first time, make sure you load the latest boot image from the Pynq web site: https://www.tulembedded.com/FPGA/ProductsPYNQ-Z2.html 

When connection a Pynq for the first time to your computer, follow these steps or go to: https://pynq.readthedocs.io/en/latest/index.html 

- Open the Network and sharing center
- Configure the connected Pynq with the IPv4: 192.168.2.1 and Subnet: 255.255.255.0
- Open web browser, connect to http://pynq:9090/tree with PW: xilinx

Loading Bitstream on Jupyter Notebook:

To load the Bitstream on Jupyter Notebook go to File -> Export -> Export Hardware. Include the Bitstream and save it to your desired location.

The generated file is “<name>.hdf”. Rename the file to “.zip” and extract it. Check that the “.bit” and “.hwh” files have the same name. Otherwise rename them. 

Open Jupyter Notebook and upload the Files (.bit, .hwh) in your Folder.

In Jupyter Notebook, load the overlay as follow:

```
from pynq import Overlay
ol = Overlay("<name>.bit")
```

## Usage
FW_SW_GULF/FW_SW_Test: BTN1 -> Reset all registers, counters, and memories

Pynq which simulates GULF: BTN0 -> Send data, BTN1 -> Reset

Access function in Jupyter_Eval_Board project:

```
from pynq import Overlay
import GULF_Eval_Board_function.master_handler as func
ol = Overlay("GULF_Eval_Board.bit")
data = func.data_handler(ol)
```
```
#Print memory content for each address
data.print_multiple_value(0, 128)
```

## Support
If you have any questions, contact:
julien.kuettel@gmail.com

